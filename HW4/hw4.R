titanic<-read.csv("titanic.csv")

#Quetion 1 & 2 - 
str(titanic)
#��� �� ���� ���
any(is.na(titanic))
#missing values: age - we will fill in all the NA's with the mean of the ages.
titanic$Age
#mean of the ages 
agemean<-mean(titanic$Age,na.rm = T)
#fill the missing values with the mean 
titanic$Age[is.na(titanic$Age)]<-agemean

 
#Q3 - Survived to factor
titanic$Survived<-factor(titanic$Survived,levels = c(0,1),lables<-c("No","Yes"))

#Q4 - Use ggplot histogram
library(ggplot2)
p1<-ggplot(titanic,aes(x=Age, fill=Survived))
p1<-p1+geom_histogram(binwidth = 1)

p2<-ggplot(titanic,aes(x=Fare, fill=Survived))+geom_histogram(binwidth = 1) 

p3<-ggplot(titanic,aes(x=SibSp, fill=Survived))+geom_histogram(binwidth = 1)


#Q5 - Select relevant features for machine learnig
#Age,SibSp, Fare

