airquality

# 1. ���� ���� ���
airquality.countnum.na <- sum(is.na(airquality))

# 2. ���� ���� ���
airquality.remove.na <- na.omit(airquality)

# 3. ����� �� �� �����
avg_measures <- c(mean(airquality.remove.na$Ozone), mean(airquality.remove.na$Solar.R), mean(airquality.remove.na$Wind), mean(airquality.remove.na$Temp))

# 4. ����� �� ������ ���� ��������� ��� ������
temp.above.avg <- airquality.remove.na[airquality.remove.na$Temp > mean(airquality.remove.na$Temp),]

# 5. ����� ��� ����� ������ ������ ����
sort.by.solar <- airquality.remove.na[order(-airquality.remove.na$Solar.R),] 

# 6. ����� ����� �� ��� ��� ����� ������ 
airquality.remove.na$ratio <- airquality.remove.na$Ozone/airquality.remove.na$Solar.R

# 7. ������ ������ �� ��� ������
par(mfcol = c(2,3), mar=rep(2,4))
plot(airquality.remove.na$Ozone,airquality.remove.na$Solar.R, main="Ozone vs Solar. R")
plot(airquality.remove.na$Ozone,airquality.remove.na$Wind, main="Ozone vs Wind")
plot(airquality.remove.na$Ozone,airquality.remove.na$Temp, main="Ozone vs Temp")
plot(airquality.remove.na$Solar.R,airquality.remove.na$Wind, main="Ozone vs Wind")
plot(airquality.remove.na$Solar.R,airquality.remove.na$Temp, main="Ozone vs Wind")
plot(airquality.remove.na$Temp,airquality.remove.na$Wind, main="Ozone vs Wind")

#Ozone & wind ����� ������